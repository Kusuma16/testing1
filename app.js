const express = require('express');
const app = express()

app.get('/', (req,res) => {
    res.send('Hello World')
})

app.get('/test', (req,res) => {
    res.send('Testing')
})

app.get('/coba', (req,res) => {
    res.send('Coba')
})

app.listen(5000, () => {
    console.log('Server is running')
})